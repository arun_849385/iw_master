<?php
namespace Deployer;

require 'vendor/instituteweb/deployer-scripts/src/common.php';

// Configuration
set('build_dir', realpath(getcwd() . '/../.builds')); // absolute path required
set('log_file', '../.builds/logs.txt'); // no dynamic variables supported here

set('git_repository', 'https://git@bitbucket.org/InstituteWeb/iw_master.git');
//set('git_repository', 'git@bitbucket.org:instituteweb/iw_master.git'); // required setted up ssh keys
//set('git_branch', 'master'); // also tags possible e.g. "1.4.2"

set('shared_files', [
    '.env'
]);
set('shared_dirs', [
    'web/fileadmin',
    'web/typo3temp',
    'web/uploads'
]);


// Servers

//server('production', 'hostname')
//    ->user('username')
//    ->password(getenv('passwd') ?: askHiddenResponse('Password for SSH: '))
//    ->set('deploy_path', '/html/typo3');
