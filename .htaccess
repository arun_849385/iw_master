# Asset Caching
<FilesMatch '\.(js|css|jpg|jpeg|png|svg|woff)$'>
	<IfModule mod_expires.c>
		ExpiresActive on
		ExpiresDefault "access plus 30 days"
	</IfModule>
	FileETag MTime Size
</FilesMatch>

# Enable gzip compression for assets
<IfModule mod_deflate.c>
	<FilesMatch '\.(js|css|jpg|jpeg|gif|svg|woff|pdf|doc|docx|xls|xlsx|pptx|pptp)$'>
		SetOutputFilter DEFLATE
	</FilesMatch>

	<IfModule mod_mime.c>
		AddEncoding gzip .gzip
		AddEncoding gzip .svgz
	</IfModule>

	<FilesMatch "\.js\.gzip$">
		AddType "text/javascript" .gzip
	</FilesMatch>
	<FilesMatch "\.css\.gzip$">
		AddType "text/css" .gzip
	</FilesMatch>
</IfModule>

# Rewrite rules
<IfModule mod_rewrite.c>
RewriteEngine On

# nc_staticfilecache Configuration
RewriteCond %{SERVER_PORT} ^443$
RewriteRule .* - [E=TX_NCSTATICFILECACHE_PROTOCOL:https]

RewriteCond %{SERVER_PORT} !^443$
RewriteRule .* - [E=TX_NCSTATICFILECACHE_PROTOCOL:http]

RewriteCond %{HTTP:Accept-Encoding} gzip [NC]
RewriteRule .* - [E=TX_NCSTATICFILECACHE_GZIP:.gz]

RewriteCond %{DOCUMENT_ROOT}/typo3temp/tx_ncstaticfilecache/%{ENV:TX_NCSTATICFILECACHE_PROTOCOL}/%{HTTP_HOST}/%{REQUEST_URI} !-f
RewriteRule .* - [E=TX_NCSTATICFILECACHE_FILE:/index.html]

RewriteCond %{QUERY_STRING} ^$
RewriteCond %{DOCUMENT_ROOT}/typo3temp/tx_ncstaticfilecache/%{ENV:TX_NCSTATICFILECACHE_PROTOCOL}/%{HTTP_HOST}/%{REQUEST_URI}%{ENV:TX_NCSTATICFILECACHE_FILE}%{ENV:TX_NCSTATICFILECACHE_GZIP} -f
RewriteCond %{HTTP_COOKIE} !nc_staticfilecache [NC]
RewriteCond %{REQUEST_METHOD} GET
RewriteCond %{HTTP_COOKIE} !be_typo_user [NC]
RewriteCond %{HTTP:Pragma} !no-cache
RewriteCond %{HTTP:Cache-Control} !no-cache
RewriteRule .* typo3temp/tx_ncstaticfilecache/%{ENV:TX_NCSTATICFILECACHE_PROTOCOL}/%{HTTP_HOST}/%{REQUEST_URI}%{ENV:TX_NCSTATICFILECACHE_FILE}%{ENV:TX_NCSTATICFILECACHE_GZIP} [L]

# Version Numbered Filenames
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.+)\.(\d+)\.(php|js|css|png|jpg|gif|gzip)$ $1.$3 [L]

# Prepend www. if no subdomain is given (but for localhost, *.local and *.vagrant)
RewriteCond %{HTTPS} off
RewriteCond %{HTTP_HOST} !\.(.*)\.
RewriteCond %{HTTP_HOST} !localhost(\:\d{2,5})?$
RewriteCond %{HTTP_HOST} !\.local(\:\d{2,5})?$
RewriteCond %{HTTP_HOST} !\.vagrant(\:\d{2,5})?$
RewriteCond %{HTTP_HOST} !\.dev(\:\d{2,5})?$
RewriteRule ^(.*)$ http://www.%{HTTP_HOST}/$1 [L,R=301]

RewriteCond %{HTTPS} on
RewriteCond %{HTTP_HOST} !\.(.*)\.
RewriteCond %{HTTP_HOST} !localhost(\:\d{2,5})?$
RewriteCond %{HTTP_HOST} !\.local(\:\d{2,5})?$
RewriteCond %{HTTP_HOST} !\.vagrant(\:\d{2,5})?$
RewriteRule ^(.*)$ https://www.%{HTTP_HOST}/$1 [L,R=301]

# Basic security checks
RewriteRule _(?:recycler|temp)_/ - [F]
RewriteRule (?:typo3conf/ext|typo3/sysext|typo3/ext)/[^/]+/(?:Configuration|Resources/Private|Tests?)/ - [F]
RewriteCond %{REQUEST_URI} "!(^|/)\.well-known/([^./]+./?)+$" [NC]
RewriteCond %{SCRIPT_FILENAME} -d [OR]
RewriteCond %{SCRIPT_FILENAME} -f
RewriteRule (?:^|/)\. - [F]

# Stop rewrite processing, if we are in the typo3/ directory.
RewriteRule ^(typo3/|fileadmin/|typo3conf/|typo3temp/|uploads/|favicon\.ico) - [L]

# If the file/symlink/directory does not exist => Redirect to index.php.
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-l
RewriteRule .* /index.php [L]

</IfModule>

# 404 error prevention for non-existing redirected folders
Options -MultiViews

# Make sure that directory listings are disabled.
<IfModule mod_autoindex.c>
	Options -Indexes
</IfModule>

<IfModule mod_headers.c>
	# Force IE to render pages in the highest available mode
	Header set X-UA-Compatible "IE=edge"
	<FilesMatch "\.(appcache|crx|css|eot|gif|htc|ico|jpe?g|js|m4a|m4v|manifest|mp4|oex|oga|ogg|ogv|otf|pdf|png|safariextz|svgz?|ttf|vcf|webapp|webm|webp|woff2?|xml|xpi)$">
		Header unset X-UA-Compatible
	</FilesMatch>

	# Reducing MIME type security risks
	Header set X-Content-Type-Options "nosniff"
</IfModule>

# Redirects
#Redirect 301 /news/aktuell http://project.tld/news/aktuelles/
