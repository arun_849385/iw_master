<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (isset($GLOBALS['TCA']['tx_gridelements_backend_layout'])) {
    $GLOBALS['TCA']['tt_content']['columns']['tx_gridelements_children']['config']['default'] = 0;
    $GLOBALS['TCA']['tt_content']['columns']['tx_gridelements_columns']['config']['default'] = 0;
    $GLOBALS['TCA']['tt_content']['columns']['tx_gridelements_container']['config']['default'] = 0;
}
