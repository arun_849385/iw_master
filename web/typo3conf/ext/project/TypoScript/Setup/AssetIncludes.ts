# Assets includes (CSS/JS)

page.includeCSS {
	default = assets/css/default.css

#	ie = assets/css/ie.css
#	ie.allWrap = <!--[if lt IE 8]>|<![endif]-->
#	ie.excludeFromConcatenation = 1

#	print = assets/css/print.css
#	print.media = print
}

# JS libraries include
page.javascriptLibs {
	jquery = 1
}

# JS includes
page.includeJSFooter {
	default = assets/js/default.js
}
