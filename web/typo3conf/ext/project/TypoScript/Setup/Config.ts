# Main configuration
config {
	# Next three parameters are for debugging only. Productive environments needs always = 0 (zero)
	no_cache = 0
	debug = 0
	admPanel = 0

	# Compression (since TYPO3 4.6)
	compressCss = 1
	concatenateCss = 1
	compressJs = 1
	concatenateJs = 1

	moveJsFromHeaderToFooter = 1

	# Checks rootline for typolinks
	typolinkCheckRootline = 1

	# Enable indexing, used for solr or indexed_search
	index_enable = 1

	# Put JS/CSS into separate file
	removeDefaultJS = external
	inlineStyle2TempFile = 1

	# Enable mail spam protection
	spamProtectEmailAddresses = -4
	spamProtectEmailAddresses_atSubst = @

	# Set up L as our language variable
	linkVars = L(int)

	# Doctype. Possible familiar values: xhtml_trans, xhtml_strict, html5, none
	doctype = html5

	xmlprologue = none
	renderCharset = utf-8
	metaCharset = utf-8

	# Enable Real URL
	tx_realurl_enable = 1
	absRefPrefix = /

	# Remove Prefix Comments
	disablePrefixComment = 1

	# Disable default rendering of page title. Title will be generated in page.ts
	noPageTitle = 2
}

#[globalVar = TSFE : beUserLogin > 0]
#	config {
#		no_cache = 1
#	}
#[global]
