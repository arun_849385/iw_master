# Navigation: Breadcrumb
lib.navigations.breadcrumb = COA
lib.navigations.breadcrumb {
	10 = COA
	10 {
		wrap = <ul>|</ul>
		10 = HMENU
		10 {
			special = rootline
			special.range = 0|-2

			1 = TMENU
			1 {
				NO {
					allWrap = <li class="first">|</li>|*|<li>|</li>
					stdWrap.htmlSpecialChars = 1
				}
			}
		}
		20 = TEXT
		20 {
			field = nav_title // title
			wrap = <li class="last"><span>|</span></li>
			htmlSpecialChars = 1
		}
	}

	stdWrap.if {
		value = 1
		equals.field = uid
		negate = 1
	}
}
