<?php

// Create Environment Object which handles system settings in environment configuration files
// based on current Application Context
$environment = \InstituteWeb\Environmental\Environment::getInstance(
    PATH_site . '../environments/',
    \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext(),
    $GLOBALS['TYPO3_CONF_VARS']
);
$GLOBALS['TYPO3_CONF_VARS'] = $environment->load();

// Append current Application Context to sitename
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] .=
    ' [' . \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext() . ']';

// Apply RealUrl configuration from UrlConfiguration.php file
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = $environment->callPhpArrayConfig(
    PATH_typo3conf . 'UrlConfiguration.php'
);
