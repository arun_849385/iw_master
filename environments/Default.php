<?php
namespace VendorName\Project;

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Environmental\Environment;

/**
 * Default Environment
 *
 * @param Environment $environment
 * @return callable function
 */
return function (Environment $environment) {
    // Environment settings
    $environment->setExtensionSetting('backend', 'loginLogo', '');
    $environment->setExtensionSetting('backend', 'loginHighlightColor', '#316d97');
    $environment->setExtensionSetting('backend', 'loginBackgroundImage', 'EXT:environmental/Resources/Public/Images/login-background.png');

    $environment->setExtensionSetting('unroll', 'unroll', '1');
    $environment->setExtensionSetting('unroll', 'hideLabelSave', '1');
    $environment->setExtensionSetting('unroll', 'hideLabelSaveDokView ', '1');
};
