<?php
namespace VendorName\Project;

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016-2017 Armin Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Environmental\Environment;

/**
 * Development Environment
 *
 * @param Environment $environment
 * @return callable function
 */
return function (Environment $environment) {
    // Environment settings
//    $environment->setDatabaseCredentials(
//        'username',
//        getenv('TYPO3_DB_PASSWORD'),
//        'db',
//        '127.0.0.1'
//    );

    $environment->typo3ConfVars['SYS']['displayErrors'] = 1;
    $environment->typo3ConfVars['SYS']['devIPmask'] = '*';
    $environment->typo3ConfVars['SYS']['sqlDebug'] = 1;
    $environment->typo3ConfVars['SYS']['enableDeprecationLog'] = 'file';
    $environment->typo3ConfVars['SYS']['errorHandlerErrors'] = E_STRICT | E_WARNING | E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE | E_RECOVERABLE_ERROR | E_DEPRECATED | E_USER_DEPRECATED;
    $environment->typo3ConfVars['SYS']['exceptionalErrors'] = E_WARNING | E_USER_ERROR | E_RECOVERABLE_ERROR | E_DEPRECATED | E_USER_DEPRECATED;
    $environment->typo3ConfVars['SYS']['systemLogLevel'] = 0;
    $environment->typo3ConfVars['SYS']['systemLog'] = 'error_log';

    $environment->typo3ConfVars['FE']['debug'] = true;
    $environment->typo3ConfVars['FE']['loginSecurityLevel'] = 'normal';

    $environment->typo3ConfVars['BE']['debug'] = true;
    $environment->typo3ConfVars['BE']['sessionTimeout'] = 60 * 60 * 12; // 12 hours
    $environment->typo3ConfVars['BE']['loginSecurityLevel'] = 'normal';

    $environment->typo3ConfVars['MAIL']['transport'] = 'mbox';
    $environment->typo3ConfVars['MAIL']['transport_mbox_file'] = dirname(PATH_site) . '/var/log/sent-mails.log';

    // TODO: disable TYPO3 caches

    $environment->typo3ConfVars['LOG']['writerConfiguration'] = [
        \TYPO3\CMS\Core\Log\LogLevel::DEBUG => [
            \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
                dirname(PATH_site) . '/var/log/typo3-default.log'
            ]
        ]
    ];

};
